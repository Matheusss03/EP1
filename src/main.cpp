#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <cstring>
#include <string>
#include <iostream>
#include <ostream>
#include <openssl/rsa.h>
#include <fstream>
#include <stdio.h>
#include "network.hpp"
#include "array.hpp"
#include "crypto.hpp"

using namespace std;

int main (int argc, const char ** argv){

    //Declaracao do meu ID
    array::array* id_cliente = array::create(8);
    id_cliente->data[0] = 0xf6;
    id_cliente->data[1] = 0xbb;
    id_cliente->data[2] = 0x6d;
    id_cliente->data[3] = 0xa7;
    id_cliente->data[4] = 0x45;
    id_cliente->data[5] = 0xf5;
    id_cliente->data[6] = 0xa0;
    id_cliente->data[7] = 0x56;

    //Criptografia do meu ID com a chave publica do servidor
    RSA *chave_publica;

    chave_publica = crypto::rsa_read_public_key_from_PEM("/home/matheus/EP1/server_pk.pem");

    array::array* id_enc;

    id_enc = crypto::rsa_encrypt(id_cliente, chave_publica);

    array::array* hash_id_enc;

    hash_id_enc = crypto::sha1(id_enc);

    //Numero do socket
    int fd;

    //Protocolo de registro
    array::array *REGISTRATION_START;

    array::array *REQUEST_REGISTRATION = array::create(7);

      REQUEST_REGISTRATION->data[0] = 0x03;
      REQUEST_REGISTRATION->data[1] = 0;
      REQUEST_REGISTRATION->data[2] = 0;
      REQUEST_REGISTRATION->data[3] = 0;
      REQUEST_REGISTRATION->data[4] = 0xC0;
      REQUEST_REGISTRATION->data[5] = 0;
      REQUEST_REGISTRATION->data[6] = 0;

      cout << "iniciando a comunicação com o servidor" << endl;

      //Conexao com o servidor
      if ((fd = network::connect("45.55.185.4", 3000)) < 0)
      {
        cout << "Falha na conexão" << fd << endl;
      }
      else {
        cout << "Conexao OK: " << fd << endl;
        network::write(fd, REQUEST_REGISTRATION);
        if((REGISTRATION_START = network::read(fd,7)) == nullptr){
          cout << "Leitura Nula" << endl;
        }
        else{
          cout << "Imprimindo conteudo do pacote REGISTRATION_START "<< REGISTRATION_START -> length << endl;
          for (int i = 0; i < ((int) REGISTRATION_START->length); i++)
          {
            printf("%X ", REGISTRATION_START->data[i]);
          }
          printf("\n");
        }
      }

      //Registro do ID no servidor
      array::array *REGISTERED;

      array::array* pacote;
      pacote = array::create(535);
      pacote-> data[0] = 0xC2;
      pacote-> data[1] = 0x00;
      pacote-> data[2] = 0x02;

      memcpy(pacote->data + 3, id_enc->data,512);

      memcpy(pacote->data + 515, hash_id_enc->data, 20);
     
      array::array *REGISTER_ID = array::create(539);
      REGISTER_ID->data[0] = 0x17;
      REGISTER_ID->data[1] = 0x02;
      REGISTER_ID->data[2] = 0;
      REGISTER_ID->data[3] = 0;

      memcpy(REGISTER_ID->data +4, pacote->data, 535);
  
        network::write(fd, REGISTER_ID);
        REGISTERED = network::read(fd,539);
          
          cout << "Imprimindo conteudo do pacote REGISTERED "<< REGISTERED -> length << endl;
          for (int i = 0; i < ((int) REGISTERED->length); i++)
          {
            printf("%X ", REGISTERED->data[i]);
          }
          printf("\n");

      RSA* chave_privada;

      chave_privada = crypto::rsa_read_private_key_from_PEM("/home/matheus/EP1/private.pem");

      array::array* chave_s;

      array::array* s_enc_inicial = array::create(512);

      memcpy(s_enc_inicial->data, REGISTERED->data + 7, 512);

      chave_s = crypto::rsa_decrypt(s_enc_inicial, chave_privada);


      //Chave simetrica salva em arquivo
      FILE* arquivo = fopen("/home/matheus/EP1/chave_s.pem","w+");
      for(int i = 0; i < ((int) chave_s->length); i++) {
        fprintf(arquivo, "%X", chave_s->data[i]);
       }
      fclose(arquivo);


     //ID criptografado novamente
     RSA *chave_publica2;

     chave_publica2 = crypto::rsa_read_public_key_from_PEM("/home/matheus/EP1/server_pk.pem");


     array::array* id_enc2;

     id_enc2 = crypto::rsa_encrypt(id_cliente, chave_publica2);
  

    array::array* hash_id_enc2;
    

    hash_id_enc2 = crypto::sha1(id_enc2);


      //Comeco da parte de autenticacao
      array::array *AUTH_START;

      array::array* pacote2;
      pacote2 = array::create(535);
      pacote2-> data[0] = 0xA0;
      pacote2-> data[1] = 0x00;
      pacote2-> data[2] = 0x02;

      memcpy(pacote2->data + 3, id_enc2->data,512);

      memcpy(pacote2->data + 515, hash_id_enc2->data, 20);
     
      array::array *REQUEST_AUTH = array::create(539);
      REQUEST_AUTH->data[0] = 0x17;
      REQUEST_AUTH->data[1] = 0x02;
      REQUEST_AUTH->data[2] = 0;
      REQUEST_AUTH->data[3] = 0;

      memcpy(REQUEST_AUTH->data +4, pacote2->data, 535);


        network::write(fd, REQUEST_AUTH);
        AUTH_START = network::read(fd,539);
         
          cout << "Imprimindo conteudo do pacote AUTH_START "<< AUTH_START -> length << endl;
          for (int i = 0; i < ((int) AUTH_START->length); i++)
          {
            printf("%X ", AUTH_START->data[i]);
          }
          printf("\n");
        

      //Obtencao do token A
      array::array* a_final;

      array::array* a_enc = array::create(512);

      memcpy(a_enc->data, AUTH_START->data + 7, 512);

      a_final = crypto::rsa_decrypt(a_enc, chave_privada);


      //Inicio do desafio de autenticacao
      array::array *CHALLENGE;

      array::array *REQUEST_CHALLENGE = array::create(7);

      REQUEST_CHALLENGE->data[0] = 0x03;
      REQUEST_CHALLENGE->data[1] = 0;
      REQUEST_CHALLENGE->data[2] = 0;
      REQUEST_CHALLENGE->data[3] = 0;
      REQUEST_CHALLENGE->data[4] = 0xA2;
      REQUEST_CHALLENGE->data[5] = 0;
      REQUEST_CHALLENGE->data[6] = 0;

      network::write(fd, REQUEST_CHALLENGE);
      CHALLENGE = network::read(fd,59);
       
          cout << "Imprimindo conteudo do pacote CHALLENGE "<< CHALLENGE -> length << endl;
          for (int i = 0; i < ((int) CHALLENGE->length); i++)
          {
            printf("%X ", CHALLENGE->data[i]);
          }
          printf("\n");
        

    //Desenvolvimento do desafio com o array M
    array::array* m_enc = array::create(32);

    memcpy(m_enc->data, CHALLENGE->data + 7, 32);

    array::array* m_final;

    m_final = crypto::aes_decrypt(m_enc, a_final, chave_s);

    array::array* hash_m_final;

    hash_m_final = crypto::sha1(m_final);


    array::array *AUTHENTICATED;

    array::array* pacote3;
    pacote3 = array::create(39);
    pacote3-> data[0] = 0xA5;
    pacote3-> data[1] = 0x10;
    pacote3-> data[2] = 0;

    memcpy(pacote3->data + 3, m_final->data,16);

    memcpy(pacote3->data + 19, hash_m_final->data, 20);
   
    array::array *AUTHENTICATE = array::create(43);
    AUTHENTICATE->data[0] = 0x27;
    AUTHENTICATE->data[1] = 0;
    AUTHENTICATE->data[2] = 0;
    AUTHENTICATE->data[3] = 0;

    memcpy(AUTHENTICATE->data +4, pacote3->data, 39);


          network::write(fd, AUTHENTICATE);
          AUTHENTICATED = network::read(fd,59);
          
            cout << "Imprimindo conteudo do pacote AUTHENTICATED "<< AUTHENTICATED -> length << endl;
            for (int i = 0; i < ((int) AUTHENTICATED->length); i++)
            {
              printf("%X ", AUTHENTICATED->data[i]);
            }
            printf("\n");


        //Obtencao do token T definitivo    
        array::array* t_enc = array::create(32);

        memcpy(t_enc->data, AUTHENTICATED->data + 7, 32);

        array::array* t_final;

        t_final = crypto::aes_decrypt(t_enc, a_final, chave_s);

        array::destroy(a_final);


        //Declaracao do ID do objeto a ser obtido
        array::array* id_objeto = array::create(8);
        id_objeto->data[0] = 0x01;
        id_objeto->data[1] = 0x02;
        id_objeto->data[2] = 0x03;
        id_objeto->data[3] = 0x04;
        id_objeto->data[4] = 0x05;
        id_objeto->data[5] = 0x06;
        id_objeto->data[6] = 0x07;
        id_objeto->data[7] = 0x08;

        //Criptografia do ID do objeto
        array::array* id_objeto_enc;

        id_objeto_enc = crypto::aes_encrypt(id_objeto, t_final, chave_s);


        array::array* hash_id_objeto;

        hash_id_objeto = crypto::sha1(id_objeto_enc);


        //Pedido do Objeto para o server
        array::array *OBJECT;

        array::array* pacote4;
        pacote4 = array::create(39);
        pacote4-> data[0] = 0xB0;
        pacote4-> data[1] = 0x10;
        pacote4-> data[2] = 0;

        memcpy(pacote4->data + 3, id_objeto_enc->data,16);

        memcpy(pacote4->data + 19, hash_id_objeto->data, 20);
       
        array::array *REQUEST_OBJECT = array::create(43);
        REQUEST_OBJECT->data[0] = 0x27;
        REQUEST_OBJECT->data[1] = 0;
        REQUEST_OBJECT->data[2] = 0;
        REQUEST_OBJECT->data[3] = 0;

        memcpy(REQUEST_OBJECT->data +4, pacote4->data, 39);

          network::write(fd, REQUEST_OBJECT);

          OBJECT = network::read(fd, 12759);
          
            cout << "Imprimindo conteudo do pacote OBJECT "<< OBJECT -> length << endl;
            for (int i = 0; i < ((int) OBJECT->length); i++)
            {
              printf("%X ", OBJECT->data[i]);
            }
            printf("\n");


        //Decriptografia do conteudo
        array::array* id_objeto_recebido = array::create(12732);

        memcpy(id_objeto_recebido->data, OBJECT->data + 7, 12732);

        array::array* objeto_final;

        objeto_final = crypto::aes_decrypt(id_objeto_recebido, t_final, chave_s); 

        //Salvando a imagem em um arquivo
        ofstream arquivo2;
        arquivo2.open ("imagem.jpeg", ios::binary);
        for (int i=0; i < ((int)objeto_final->length); i++)
        {  
          arquivo2 << objeto_final->data[i];
        } 
        arquivo2.close();

      
  return 0;    
}



