Universidade de Brasília-FGA
Matheus Figueiredo
14/0047743
Orientação a Objetos
Renato Sampaio

Programa de comunicação entre cliente e servidor usando técnicas de segurança (Criptografia)
Para conseguir rodar o programa faça os seguintes passos:
--Faça o dowmload do diretório pelo seguinte endereço: https://gitlab.com/Matheusss03/EP1.git
-- Abra o diretório: EP1;
--Pelo terminal use os seguintes comandos:
make clean
make make run
--Após isso no arquivo "foto" presente na raiz do diretório vai estar presente a imagem baixada do servidor.

OBS: Devido as muitas dificuldades encontradas, infelizmente, nao consegui implementar o principal intuito da disciplina, no caso o encapsulamento, por isso o codigo está todo presente na main. Houve partes em que eu fiquei "travado" por muito tempo, e só consegui obter a imagem no ultima dia, com isso nao daria tempo de implementar as classes.
