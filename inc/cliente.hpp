#ifndef CLIENTE_H
#define CLIENTE_H

#include <iostream>
#include <string>

using namespace std;

class Cliente{
private:
	string ID_Cliente;

public:
	Cliente();
	Cliente(string ID_Cliente);
	string getID_Cliente();
	void setID_Cliente(string ID_Cliente);
};

#endif
